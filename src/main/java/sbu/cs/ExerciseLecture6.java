package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class ExerciseLecture6
{

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr)
    {
        long sum = 0;

        for(int len = 0; len < arr.length; len+=2)
        {
            sum += arr[len];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr)
    {
        int [] reverse = new int [arr.length];

        for(int i = arr.length-1, j = 0; i >= 0; i--, j++)
        {
            reverse[j] = arr[i];
        }
        return reverse;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException
    {
        if(m2.length != m1[0].length)
        {
            throw new IllegalValueException();
        }
        double[][] product = new double[m1.length][m2[0].length];

        for(int i = 0; i < m1.length; i++)
        {
            for (int j = 0; j < m2[0].length; j++)
            {
                for (int k = 0; k < m1[0].length; k++)
                {
                    product[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        return product;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names)
    {
        List<List<String>> namesList = new ArrayList<List<String>>();

        for(int i = 0; i < names.length; i++)
        {
            ArrayList<String> temp = new ArrayList<>();
            for(int j = 0; j < names[0].length; j++)
            {
                temp.add(names[i][j]);
            }
            namesList.add(temp);
        }
        return namesList;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> primeFactors = new ArrayList<Integer>();

        if (n == 1)
        {
            return primeFactors;
        }

        for (int i = 2 ; i <= n ; i++)
        {
            int temp = 0;

            while (n % i == 0)
            {
                n /= i;
                temp ++;
            }

            if (temp > 0)
            {
                primeFactors.add(i);
            }
        }

        return primeFactors;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line)
    {
        line = line.replaceAll("[!@#$%^&*()?\\-_~,/]*", "");
        String [] words = line.split(" ");

        List<String> str = new ArrayList<String>();

        for(int i = 0; i < words.length; i++)
        {
            str.add(words[i]);
        }

        return str;
    }
}
