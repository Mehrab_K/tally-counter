package sbu.cs;

import java.util.*;

public class ExerciseLecture5
{

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length)
    {
        StringBuilder str = new StringBuilder();
        Random rand = new Random();

        for(int i = 0; i < length; i++)
            str.append((char)(rand.nextInt(26) + 'a'));

        return str.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception
    {
        if(length < 3)
        {
            throw new IllegalValueException();
        }
        String special = "@#$/*&)(!~";

        Random rand = new Random();
        int random = rand.nextInt(10);

        StringBuilder pass = new StringBuilder();
        pass.append(random).append(special.charAt(random));

        for(int i = 0; i < length-2; i++)
            pass.append((char)(rand.nextInt(26) + 'a'));

        return pass.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n)
    {
        ExerciseLecture4 obj = new ExerciseLecture4();
        long fib;
        int counter = 1;

        do
        {
            fib = obj.fibonacci(counter);
            int bin = Long.toBinaryString(fib).replaceAll("0", "").length();

            if((bin + fib) == n)
            {
                return true;
            }
            counter++;
        }
        while(fib < n);


        return false;
    }
}
