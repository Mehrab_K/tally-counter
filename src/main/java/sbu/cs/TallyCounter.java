package sbu.cs;

public class TallyCounter implements TallyCounterInterface
{
    private int value = 0;

    @Override
    public void count()
    {
        if(value != 9999)
            value++;
    }

    @Override
    public int getValue()
    {
        return value;
    }

    @Override
    public void setValue(int value) throws IllegalValueException
    {
        if(value < 0 || value > 9999)
            throw new IllegalValueException();
        else
            this.value = value;
    }
}
